import 'package:flutter/material.dart';
import 'package:flutter_starter/mocks/mock_location.dart';
import 'details.dart';

void main() => runApp(MaterialApp(home: Details(MockLocation.fetchAny())));
