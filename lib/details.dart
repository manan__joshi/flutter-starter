import 'package:flutter/material.dart';
import 'package:flutter_starter/models/location.dart';

class Details extends StatelessWidget {
  final Location location;

  Details(this.location);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(location.name),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: _renderFacts(location.url, 172.0, location.facts),
        ));
  }

  List<Widget> _renderFacts(String url, double height, List facts) {
    return facts.fold([_bannerImage(url, height)], (acc, curr) {
      acc.add(Container(
          padding: EdgeInsets.fromLTRB(10, 12, 12, 12),
          child: Text(
            curr.title,
            textAlign: TextAlign.left,
            style: TextStyle(color: Colors.blueGrey, fontSize: 24.0),
          )));
      acc.add(Text(curr.desc));
      return acc;
    });
  }

  Widget _bannerImage(String url, double height) {
    return Container(
        constraints: BoxConstraints.tightFor(height: height),
        child: Image.network(
          url,
          fit: BoxFit.fitWidth,
        ));
  }
}
